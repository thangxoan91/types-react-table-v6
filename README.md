# Installation
> `npm install --save @types/react-table`

# Summary
This package contains type definitions for react-table (https://github.com/react-tools/react-table).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-table

Additional Details
 * Last updated: Mon, 22 Jul 2019 20:06:44 GMT
 * Dependencies: @types/react
 * Global values: none

# Credits
These definitions were written by Roy Xue <https://github.com/royxue>, Pavel Sakalo <https://github.com/psakalo>, Krzysztof Porębski <https://github.com/Havret>, Andy S <https://github.com/andys8>, Grzegorz Rozdzialik <https://github.com/Gelio>, and Cam Pepin <https://github.com/cpepin>.
